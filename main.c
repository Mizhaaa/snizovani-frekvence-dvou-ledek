/* vzorový projekt (sem pište stručný popis projektu)

*/

#include "stm8s.h"
#include "milis.h"
#include "stm8_hd44780.h"
#include "swi2c.h"

uint16_t frekvence1 = 250;
uint16_t frekvence2 = 250;
uint16_t last_time1 = 0;
uint16_t last_time2 = 0;




void main(void){
CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1); // 16MHz z interního RC oscilátoru
GPIO_Init(GPIOD, GPIO_PIN_5, GPIO_MODE_OUT_PP_HIGH_SLOW); //
GPIO_Init(GPIOD, GPIO_PIN_6, GPIO_MODE_OUT_PP_HIGH_SLOW);
GPIO_Init(GPIOG, GPIO_PIN_1, GPIO_MODE_IN_FL_NO_IT); // TL 2
GPIO_Init(GPIOG, GPIO_PIN_2, GPIO_MODE_IN_FL_NO_IT); // TL 1
init_milis();



  while (1){

	if(milis() - last_time1 >= frekvence1)
	{
    last_time1 = milis(); // uložíme si čas abychom věděli kdy přepnout LED příště
    GPIO_WriteReverse(GPIOD,GPIO_PIN_5); // přepneme stav LEDky
  }
	
	if(milis() - last_time2 >= frekvence2)
	{
    last_time2 = milis(); // uložíme si čas abychom věděli kdy přepnout LED příště
    GPIO_WriteReverse(GPIOD,GPIO_PIN_6); // přepneme stav LEDky
  }


	
	if(GPIO_ReadInputPin(GPIOG,GPIO_PIN_2)==RESET)
	{ // pokud je na vstupu od "ON" tlačítka log.0 tak...
   GPIO_WriteReverse(GPIOD,GPIO_PIN_5);
	 while(GPIO_ReadInputPin(GPIOG,GPIO_PIN_1)==RESET);// ...je tlačítko stisknuté
	 frekvence1+=250;
	 if (frekvence1>3000)
	 { 
		frekvence1 = 250;
	 }
  }
	
	
	
	if(GPIO_ReadInputPin(GPIOG,GPIO_PIN_1)==RESET)
	{ // pokud je na vstupu od "ON" tlačítka log.0 tak...
   GPIO_WriteReverse(GPIOD,GPIO_PIN_6);
	 while(GPIO_ReadInputPin(GPIOG,GPIO_PIN_2)==RESET);// ...je tlačítko stisknuté
	 frekvence2+=250;
	 if (frekvence2>3000)
	 { 
		frekvence2 = 250;
	 }
  }
	
  }
	
}


// pod tímto komentářem nic neměňte 
#ifdef USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval : None
  */
void assert_failed(u8* file, u32 line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif
